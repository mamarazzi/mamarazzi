package com.mamarazzi.game;

import java.util.ArrayList;


import com.mamarazzi.game.classes.Mission;
import com.mamarazzi.game.classes.MissionListAdapter;
import com.mamarazzi.game.classes.MissionManager;
import com.mamarazzi.game.classes.User;
import com.mamarazzi.game.interfaces.MissionListOnClickListener;
import com.mamarazzi.game.activities.MainActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

@SuppressLint("NewApi")
public class MissionFragment extends Fragment 
implements MissionManager.MissionDatabaseListener {
	
	private MissionManager _missionManager = null;
	private MissionListAdapter _missionAdapter = null;
	private ArrayList<Mission> _missionList = null;
	private MainActivity _context = null;
	private User _activeUser = null;
	
	
	@Override
	public void onAttach (Activity activity) {
		super.onAttach(activity);
		_context = (MainActivity) activity;
		

	}
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		_missionManager = new MissionManager(_context);
		_missionManager.open();
		_missionManager.addMissionDatabaseListener(this);
		_missionManager.requestUpdate();
		
		_missionList = _missionManager.getMissionList(-1, -1);
		
		for (Mission mission : _missionList) {
			Log.d("Missiosnsonsd: ", "" + mission.getId());
		}
		
		Log.d("onCreate", "_missionList: " + _missionList);
		Log.d("onCreate", "_activeUser: " + _activeUser);
		_missionAdapter = new MissionListAdapter(_context, R.layout.mission_entry_incomplete_layout, _missionList);
		_missionAdapter.setOnMissionListClickListener((MissionListOnClickListener) getActivity());
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		ListView listView = (ListView) inflater.inflate(R.layout.mission_fragment_layout, container, false);
		listView.setAdapter(_missionAdapter);
		return listView;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		_missionManager.close();
	}
	
	public MissionManager getMissionManager() {
		return _missionManager;
	}
	
	public void refreshMissionList() {
		_missionAdapter.clear();
		ArrayList<Mission> list = _missionManager.getMissionList(-1, -1);
		for (Mission mission : list) {
			_missionAdapter.add(mission);
		}
		
		_missionAdapter.notifyDataSetChanged();
	}

	@Override
	public void onMissionDatabaseChanged() {
		refreshMissionList();
		
	}
}
