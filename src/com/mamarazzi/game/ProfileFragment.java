package com.mamarazzi.game;

import com.mamarazzi.game.classes.User;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class ProfileFragment extends Fragment {

	private User _activeUser = null;
	
	public ProfileFragment() {
	}
	
	public void setActiveUser(User user) {
		_activeUser = user;
	}
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.d("ProfileFragment", "onCreate");
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		RelativeLayout returnView = (RelativeLayout) inflater.inflate(R.layout.profile_fragment_layout, container, false);
		
		
		return returnView;
	}
}
