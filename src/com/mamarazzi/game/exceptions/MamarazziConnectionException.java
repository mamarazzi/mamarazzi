package com.mamarazzi.game.exceptions;

public class MamarazziConnectionException extends MamarazziException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MamarazziConnectionException() {
		super();
	}
	
	public MamarazziConnectionException(Exception e) {
		super(e);
	}

}
