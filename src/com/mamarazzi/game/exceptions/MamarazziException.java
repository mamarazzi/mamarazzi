package com.mamarazzi.game.exceptions;

public class MamarazziException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MamarazziException() {
		super();
	}
	
	public MamarazziException(Exception e) {
		super(e);
	}

}
