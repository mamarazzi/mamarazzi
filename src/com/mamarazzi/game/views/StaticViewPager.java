package com.mamarazzi.game.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;




public class StaticViewPager extends ViewPager {

	public StaticViewPager(Context context) {
		super(context);
	}
	
	public StaticViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	
	@Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

}
