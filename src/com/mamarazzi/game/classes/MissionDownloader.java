package com.mamarazzi.game.classes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.os.AsyncTask;
import android.util.Log;

public class MissionDownloader extends AsyncTask<URI, Integer,  MissionDownloader.Response> {
	
	private static final String DEFAULT_RESPONSE = "{\"request\":\"list\",\"success\":1,\"missions\":[]}";
	
	public static final String KEY_MISSIONS = "missions";
	public static final String KEY_SUCCESS = "success";
	public static final String KEY_REQUEST = "request";
	public static final String MISSIONKEY_ID = "mission_id";
	public static final String MISSIONKEY_TITLE = "title";
	public static final String MISSIONKEY_TYPE = "type";
	public static final String MISSIONKEY_LEVEL = "level";
	
	private MissionDownloadListener _downloadListener = null;
	private ArrayList<Integer> _exclusionList = new ArrayList<Integer>();
		
	public MissionDownloader() {
		super();
	}
	
	@Override
	protected MissionDownloader.Response doInBackground(URI... uri) {
		Log.d("doInBackground", "beginning");
		
		HttpResponse httpResponse;
		try {
			httpResponse = requestMissions(uri[0]);
		} catch (MamarazziConnectionException e) {
			e.printStackTrace();
			return new MissionDownloader.Response(DEFAULT_RESPONSE);
		}
		
		String responseMessage = responseToString(httpResponse);
		
		Log.d("doInBackground", "responseMessage: " + responseMessage);
		
		Log.d("doInBackground", "createResponse");
		MissionDownloader.Response response = new MissionDownloader.Response(responseMessage);
		Log.d("doInBackground", "createdResponse");
		
		Log.d("doInBackground", "exiting");
		
		return response;
	}
	
	@Override
	protected void onPostExecute(MissionDownloader.Response response) {
		super.onPostExecute(response);
		_downloadListener.onMissionDownloadComplete(response);
	}
	
	public void setMissionDownloadListener(MissionDownloadListener listener) {
		_downloadListener = listener;
	}
	
	public void setExclusionList(ArrayList<Integer> exclusionList) {
		_exclusionList = exclusionList;
	}
	
	private String exclusionListToJSONString(ArrayList<Integer> exclusionList) {
		Log.d("exclusionListToJSONString: ", "exclusionList.length: " + exclusionList.size());
		JSONArray jsonArray = new JSONArray();
		for (Integer id : exclusionList) {
			Log.d("EXLUSION LIST: ", "ID: " + id);
			jsonArray.put(id);
		}
		
		return jsonArray.toString();
	}
	
	private HttpResponse requestMissions(URI uri) throws MamarazziConnectionException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(uri);
		
		ArrayList<BasicNameValuePair> formData = new ArrayList<BasicNameValuePair>();
		String exclusionList = exclusionListToJSONString(_exclusionList);
		
		Log.d("exclusionList JsonArray", "Array: " + exclusionList);
		formData.add(new BasicNameValuePair("request", "list"));
		formData.add(new BasicNameValuePair("exclude", exclusionList));
		
		
		
		HttpResponse httpResponse = null;
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(formData));
			httpResponse = httpClient.execute(httpPost);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new MamarazziConnectionException(e);
		}
		
		return httpResponse;
	}
	
	private String responseToString(HttpResponse response) {
		StringBuilder responseBuilder = new StringBuilder();
		try {
			HttpEntity responseEntity = response.getEntity();
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
			String line = "";
			while ((line = responseReader.readLine()) != null) {
				responseBuilder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return responseBuilder.toString();
	}

	
	public interface MissionDownloadListener {
		void onMissionDownloadComplete(MissionDownloader.Response response);
	}
	
	public class Response {
		private String _request = "";
		private int _success = 0;
		private ArrayList<Mission> _missions = null;
		
		public Response(String response) {
			
			JSONObject json = null;
			try {
				json = new JSONObject(response);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			try {
				_request = json.getString(KEY_REQUEST);
				_success = json.getInt(KEY_SUCCESS);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
			
			if (_success == 1) {
				_missions = new ArrayList<Mission>();
				JSONArray jsonArray = null;
				try {
					jsonArray = json.getJSONArray(KEY_MISSIONS);
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				
				for (int i = 0; i < jsonArray.length(); i++) {
					Mission mission = new Mission();
					try {
						mission.setId(jsonArray.getJSONObject(i).getInt(MissionDownloader.MISSIONKEY_ID));
						mission.setTitle(jsonArray.getJSONObject(i).getString(MissionDownloader.MISSIONKEY_TITLE));
						mission.setType(jsonArray.getJSONObject(i).getInt(MissionDownloader.MISSIONKEY_TYPE));
						mission.setLevelRequirement(jsonArray.getJSONObject(i).getInt(MissionDownloader.MISSIONKEY_LEVEL));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					_missions.add(mission);
				}
			}
		}
	
		public String getRequest() {
			return _request;
		}
		
		public int successful() {
			return _success;
		}
		
		public ArrayList<Mission> getMissions() {
			return _missions;
		}
	}

}

