package com.mamarazzi.game.classes;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.os.AsyncTask;
import android.util.Log;

public class PictureMetaDownloadTask extends AsyncTask<Void, Void, PictureMetaDownloadResponse> {
	
	private OnPictureMetaDownloadedListener _downloadListener = null;
	private static final String DEFAULT_RESPONSE = "{\"request\":\"get\",\"sucess\":false,\"pictures\":[]}";
	
	public PictureMetaDownloadTask() {
		
	}
	
	public void setOnPictureMetaDownloadedListener(OnPictureMetaDownloadedListener listener) {
		_downloadListener = listener;
	}
	
	
	@Override
	protected PictureMetaDownloadResponse doInBackground(Void... arg0) {
		PictureDatabaseManager manager = new PictureDatabaseManager();
		PictureMetaDownloadResponse response;
		try {
			response = manager.getPictureMeta();
		} catch (MamarazziConnectionException e) {
			return new PictureMetaDownloadResponse(DEFAULT_RESPONSE);
		}
		return response;
	}
	
	@Override
	protected void onPostExecute(PictureMetaDownloadResponse result) {
		_downloadListener.onPictureMetaDownloaded(result);
	}
	
	public interface OnPictureMetaDownloadedListener {
		public void onPictureMetaDownloaded(PictureMetaDownloadResponse result);
	}

}
