package com.mamarazzi.game.classes;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;

@SuppressWarnings("rawtypes")
public class MissionView extends AdapterView {

	Adapter _adapter = null;
	
	int _listTop = 0;
	int _tempTop = 0;
	int _touchStartY = 0;
	int _touchEndY = 0;
	
	public MissionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}

	@Override
	public Adapter getAdapter() {
		return _adapter;
	}

	@Override
	public View getSelectedView() {
		return null;
	}

	@Override
	public void setAdapter(Adapter adapter) {
		_adapter = adapter;
		removeAllViewsInLayout();
		requestLayout();
	}

	@Override
	public void setSelection(int position) {
		
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		
		if (_adapter == null) {
			Log.d("onLayout", "_adapter: " + _adapter); 
			return;
		}
		
		if (getChildCount() == 0) {
			int position = 0;
			int verticalSpace = 0;
			while (verticalSpace < getHeight() && position < _adapter.getCount()) {
				Log.d("onLayout", "verticalSpaceLeft: " + verticalSpace);
				Log.d("onLayout", "_adapter.getCount(): " + _adapter.getCount());
				
				View child = _adapter.getView(position, null, this);
				
				LayoutParams params = child.getLayoutParams();
				if (params == null) {
					params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				}
				
				addViewInLayout(child, -1, params, true);
				
				child.measure(MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY), MeasureSpec.UNSPECIFIED);
				
				verticalSpace += child.getMeasuredHeight();
				position++;
			}
		}
		
		
		int childTop = _listTop;
		
		for (int index = 0; index < getChildCount(); index++) {
			View newChild = getChildAt(index);
			
			int measuredWidth = newChild.getMeasuredWidth();
			int measuredHeight = newChild.getMeasuredHeight();
			int childBottom = childTop + measuredHeight;
			int childLeft = (getWidth() - measuredWidth) / 2;
			int childRight = childLeft + measuredWidth;
			
			newChild.layout(childLeft, childTop, childRight, childBottom);
			
			childTop += measuredHeight + 25;
		}	
	}
	
	@Override
	protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
		int childMidX = child.getWidth() / 2;
		int childMidY = child.getHeight() / 2;
		
		int childPosX = child.getLeft() + childMidX;
		int childPosY = child.getTop() + childMidY;
		
		Log.d("childPosX", childPosX + "");
		Log.d("childPosY", childPosY + "");
		
		int screenCenter = getHeight() / 2;
		float distanceFromCenter = (((float)childPosY - (float)screenCenter) / (float)screenCenter);
		
		Log.d("distanceFromCenter", "" + distanceFromCenter);
		
		float scale = (float) (1 * Math.cos(distanceFromCenter));
		canvas.save();
		canvas.scale(scale, scale, childPosX, childPosY);
		canvas.translate(-2 * childPosX * (1 - scale), 0);
		super.drawChild(canvas, child, drawingTime);
		canvas.restore();
		
		return false;
	}

	
	@Override
	public boolean onTouchEvent (MotionEvent event) {
		if (getChildCount() == 0) {
			return false;
		}
		
		
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				_touchStartY = (int)event.getY();
				_tempTop = _listTop;
				break;
			case MotionEvent.ACTION_MOVE:
				_touchEndY = (int)event.getY();
				if (_touchEndY != _touchStartY) {
					int distance = _touchEndY - _touchStartY;
					_listTop = _tempTop + distance;
					requestLayout();
				}
				break;
		}		
		return true;
	}
}
