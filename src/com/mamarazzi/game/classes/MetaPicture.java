package com.mamarazzi.game.classes;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

public class MetaPicture {

	private static final String KEY_ID = "id";
	private static final String KEY_AUTHOR = "author_id";
	private static final String KEY_MISSION = "mission_id";
	private static final String KEY_USERNAME = "username";
	private static final String KEY_TITLE = "title";
	private static final String KEY_PATH = "image_path";
	
	
	private int _id = -1;
	private int _author = -1;
	private int _mission = -1;
	private String _username = "";
	private String _imagePath = "";
	private String _title = "";
	private List<Comment> _comments = new ArrayList<Comment>();
	private OnCommentsUpdatedListener _updatedListener = null;
	
	
	public MetaPicture(String response) {
		JSONObject json = null;
		try {
			json = new JSONObject(response);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			_id = json.getInt(KEY_ID);
			_author = json.getInt(KEY_AUTHOR);
			_mission = json.getInt(KEY_MISSION);
			_username = json.getString(KEY_USERNAME);
			_imagePath = json.getString(KEY_PATH);
			_title = json.getString(KEY_TITLE);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void setCommentUpdatedListener(OnCommentsUpdatedListener listener) {
		_updatedListener = listener;
	}
	
	public int getId() {
		return _id;
	}
	
	public int getAuthor() {
		return _author;
	}
	
	public int getMission() {
		return _mission;
	}
	
	public String getUsername() {
		return _username;
	}
	
	public String getImagePath() {
		return _imagePath;
	}
	
	public String getTitle() {
		return _title;
	}
	
	public void setComments(List<Comment> comments) {
		_comments = comments;
		if (_updatedListener != null)
			_updatedListener.onCommentsUpdatedListener(_id);
	}
	
	public List<Comment> getComments() {
		return _comments;
	}
	
	public interface OnCommentsUpdatedListener {
		public void onCommentsUpdatedListener(int pictureId);
	}
	
}
