package com.mamarazzi.game.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class Comment {
	private static final String KEY_ID = "id";
	private static final String KEY_PICTURE = "picture_id";
	private static final String KEY_USER = "user_id";
	private static final String KEY_USERNAME = "username";
	private static final String KEY_COMMENT = "comment";
	private static final String KEY_CREATED = "created_date";
	
	private int _id;
	private int _pictureId;
	private int _userId;
	private String _username;
	private String _comment;
	private String _createdDate;
	
	public Comment(String response) {
		JSONObject json = null;
		try {
			json = new JSONObject(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			_id = json.getInt(KEY_ID);
			_pictureId = json.getInt(KEY_PICTURE);
			_userId = json.getInt(KEY_USER);
			_username = json.getString(KEY_USERNAME);
			_comment = json.getString(KEY_COMMENT);
			_createdDate = json.getString(KEY_CREATED);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	public int getId() {
		return _id;
	}
	
	public int getPictureId() {
		return _pictureId;
	}
	
	public int getUserId() {
		return _userId;
	}
	
	public String getUsername() {
		return _username;
	}
	
	public String getComment() {
		return _comment;
	}
	
	public String getCreatedDate() {
		return _createdDate;
	}
	
	
	
}
