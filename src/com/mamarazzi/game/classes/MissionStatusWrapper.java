
package com.mamarazzi.game.classes;

public class MissionStatusWrapper {
	
	public final static int STATUS_INCOMPLETE = 0;
	public final static int STATUS_COMPLETE = 1;
	
	private Mission _missionEntry = null;
	private int _status = STATUS_INCOMPLETE;
	
	
	public MissionStatusWrapper() {
	}
	
	public MissionStatusWrapper(Mission entry, int status) {
		_missionEntry = entry;
		_status = status;
	}

	public void setMissionEntry(Mission entry) {
		_missionEntry = entry;
	}
	
	public Mission getMissionEntry() {
		return _missionEntry;
	}
	
	public void setStatus(int status) {
		_status = status;
	}
	
	public int getStatus() {
		return _status;
	}
}
