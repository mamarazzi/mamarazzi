package com.mamarazzi.game.classes;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

public class User {
	private static int _id = -1;
	private static String _username = "";
	private static String _email = "";
	private static int _experience = 0;
	private static ArrayList<Integer> _completion = null;
	private static String _profileImageUrl = "";
	
	public static void setUniqueId(int id) {
		_id = id;
	}
	
	public static int getId() {
		return _id;
	}
	
	public static void setUsername(String username) {
		_username = username;
	}
	
	public static String getUsername() {
		return _username;
	}
	
	public static void setEmail(String email) {
		_email = email;
	}
	
	public static String getEmail() {
		return _email;
	}
	
	public static void setExperience(int experience) {
		_experience = experience;
	}
	
	public static void addExperience(int experience) {
		_experience += experience;
	}
	
	public static int getExperience() {
		return _experience;
	}
	
	public static void setCompletion(String completion) {
		ArrayList<Integer> completionList = new ArrayList<Integer>();
		
		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(completion);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		for (int i = 0; i < jsonArray.length(); i++) {
			try {
				completionList.add(jsonArray.getInt(i));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		_completion = completionList;
	}
	
	public static void setCompletion(ArrayList<Integer> completion) {
		_completion = completion;
	}
	
	public static ArrayList<Integer> getCompletion() {
		return _completion;
	}
	
	public static void addCompletedMission(int id) {
		if (_completion.contains(id) == false) {
			_completion.add(id);
		}
	}
	
	public static void setProfileImageUrl(String profileImageUrl) {
		_profileImageUrl = profileImageUrl;
	}
	
	public static String getProfileImageUrl() {
		return _profileImageUrl;
	}
	
	public static boolean hasCompleted(int id) {
		for (int i  : _completion) {
			if (i == id)
				return true;
		}
			
		return false;
	}
	
}
	