package com.mamarazzi.game.classes;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalSessionManager {
	
	private SharedPreferences _preferences = null;
	
	private static final String SESSION_FILE = "session_data";
	private static final String USERNAME_KEY = "username";
	private static final String EMAIL_KEY = "email";
	private static final String UNIQUE_KEY = "unique_id";
	private static final String COMPLETED_KEY = "completed";
	private static final String EXPERIENCE_KEY = "experience";
	private static final String PROFILEIMAGE_KEY = "profile_image";
	
	public LocalSessionManager(Context context) {
		_preferences = context.getSharedPreferences(SESSION_FILE, Context.MODE_PRIVATE);
	}
	
	public void clearAll() {
		SharedPreferences.Editor editor = _preferences.edit();
		editor.clear();
		editor.commit();
	}
	
	public void getUsername() {
		_preferences.getString(USERNAME_KEY, "");
	}
	
	public String getEmail() {
		return _preferences.getString(EMAIL_KEY, "");
	}
	
	public String getUniqueId() {
		return _preferences.getString(UNIQUE_KEY, "");
	}
	
	public String getCompleted() {
		return _preferences.getString(COMPLETED_KEY, "");
	}
	
	public int getExperience() {
		return _preferences.getInt(EXPERIENCE_KEY, -1);
	}
	
	public String getProfileImageUrl() {
		return _preferences.getString(PROFILEIMAGE_KEY, "");
	}
	
	public void setUsername(String username) {
		SharedPreferences.Editor editor = _preferences.edit();
		editor.putString(USERNAME_KEY, username);
		editor.commit();
	}
	
	public void setEmail(String email) {
		SharedPreferences.Editor editor = _preferences.edit();
		editor.putString(EMAIL_KEY, email);
		editor.commit();
	}
	
	public void setUniqueId(String uniqueId) {
		SharedPreferences.Editor editor = _preferences.edit();
		editor.putString(UNIQUE_KEY, uniqueId);
		editor.commit();
	}
	
	public void setCompleted(String completed) {
		SharedPreferences.Editor editor = _preferences.edit();
		editor.putString(COMPLETED_KEY, completed);
		editor.commit();
	}
	
	public void setExperience(int experience) {
		SharedPreferences.Editor editor = _preferences.edit();
		editor.putInt(EXPERIENCE_KEY, experience);
		editor.commit();
	}
	
	public void setProfileImageUrl(String profileImageUrl) {
		SharedPreferences.Editor editor = _preferences.edit();
		editor.putString(PROFILEIMAGE_KEY, profileImageUrl);
		editor.commit();
	}
	
	
}
