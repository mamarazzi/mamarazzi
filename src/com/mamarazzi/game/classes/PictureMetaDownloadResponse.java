package com.mamarazzi.game.classes;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PictureMetaDownloadResponse {

	private static final String KEY_REQUEST = "request";
	private static final String KEY_SUCCESS= "success";
	private static final String KEY_IMAGES = "pictures";

	
	
	private String _request = "";
	private boolean _success = false;
	private ArrayList<MetaPicture> _pictures = new ArrayList<MetaPicture>();
	
	
	public PictureMetaDownloadResponse(String response) {
		JSONObject json = null;
		try {
			json = new JSONObject(response);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			_request = json.getString(KEY_REQUEST);
			_success = json.getBoolean(KEY_SUCCESS);
			
			if (_success) {
				JSONArray imageArray = json.getJSONArray(KEY_IMAGES);
				
				for (int i = 0; i < imageArray.length(); i++) {
					MetaPicture picture = new MetaPicture(imageArray.get(i).toString());
					_pictures.add(picture);
				}
			}
			else {
				
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	public String getRequest() {
		return _request;
	}
	
	public boolean successful() {
		return _success;
	}
	
	public ArrayList<MetaPicture> getMetaPictures() {
		return _pictures;
	}
	
	
	
}
