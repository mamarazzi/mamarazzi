package com.mamarazzi.game.classes;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mamarazzi.game.R;
import com.mamarazzi.game.interfaces.MissionListOnClickListener;

@SuppressWarnings("rawtypes")
public class MissionListAdapter extends ArrayAdapter<Mission> {
	
	private int _resource;
	private LayoutInflater _inflater;
	private Context _context;
	private Drawable _incompleteBackground = null;
	private Drawable _completeBackground = null;
	private Drawable _incompleteButtonBackground = null;
	private Typeface _titleTypeface = null;
	private MissionListOnClickListener _missionListOnClickListener = null;
	
	@SuppressWarnings("unchecked")
	public MissionListAdapter(Context context, int resourceId, List<Mission> objects) {
		super(context, resourceId, objects);
		_resource = resourceId;
		_inflater = LayoutInflater.from(context);
		_context = context;
		
		initDrawables();
	}
	
	public void setOnMissionListClickListener(MissionListOnClickListener listener) {
		_missionListOnClickListener = listener;
	}
	
	
	// Preparing drawables for layout once as to not slow down the app by having to recreate the drawables during each call to get a view.
	private void initDrawables() {
		_titleTypeface = Typeface.createFromAsset(_context.getAssets(), "Gross Regular.ttf");
		 
		Resources res = _context.getResources();
		_incompleteBackground = res.getDrawable(R.drawable.missionbar);
		_completeBackground = res.getDrawable(R.drawable.mission_completed);
		_incompleteButtonBackground = res.getDrawable(R.drawable.missionbutton);
	}
	
	// Used for listening to click events. Clicking on the item at position 0 will ask if it is enabled.
	// If isEnabled returns false, the onClick event is not resolved, preventing the application from crashing.
	// Very sensitive, do not change without knowing what you're doing.
	@Override
	public boolean isEnabled(int position) {
		if (isCompleted(((Mission)this.getItem(position)).getId())) {
			return false;
		}
		
		return true;
	}
	
	// Returns false whenever list is asked whether all items are enabled or not.
	// Should remain the same.
	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}
	
	
	private boolean isCompleted(int missionId) {
		for (int i = 0; i < User.getCompletion().size(); i++) {
			if (missionId == User.getCompletion().get(i)) {
				return true;
			}
		}
		
		return false;
	}
	
	// Main layout function for the list view items.
	// Any changes to individual list entries goes here.
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
    public View getView ( int position, View convertView, ViewGroup parent ) {
		Mission mission = (Mission) getItem(position);
		convertView = (RelativeLayout) _inflater.inflate(_resource, null);
		
		
		
		
		RelativeLayout relativeLayout = (RelativeLayout) convertView.findViewById(R.id.mission_entry);
		Button buttonPicture = (Button) convertView.findViewById(R.id.mission_layout_button_pic);
		Button buttonGallery = (Button) convertView.findViewById(R.id.mission_layout_button_gal);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.mission_name);
		
		
		if (User.hasCompleted(mission.getId())) {
			buttonPicture.setVisibility(View.GONE);
			buttonGallery.setVisibility(View.GONE);
			convertView.setClickable(false);
			convertView.setFocusable(false);
		
			buttonPicture.setClickable(false);
			buttonPicture.setFocusable(false);
			buttonGallery.setClickable(false);
			buttonGallery.setFocusable(false);
			
			if (Build.VERSION.SDK_INT >= 16) {
				relativeLayout.setBackground(_completeBackground);
			}
			
			else {
				relativeLayout.setBackgroundDrawable(_completeBackground);
			}
			
		}
		else {
			if (Build.VERSION.SDK_INT >= 16) {
				relativeLayout.setBackground(_incompleteBackground);
				buttonPicture.setBackground(_incompleteButtonBackground);
				buttonGallery.setBackground(_incompleteButtonBackground);
			}
			else {
				relativeLayout.setBackgroundDrawable(_incompleteBackground);
				buttonPicture.setBackgroundDrawable(_incompleteButtonBackground);
				buttonGallery.setBackgroundDrawable(_incompleteButtonBackground);
			}
		}
		 
		 
		 
		txtTitle.setTypeface(_titleTypeface);
		txtTitle.setTextSize(12);
		txtTitle.setText(mission.getTitle());

		
		
		
		
		buttonPicture.setTypeface(_titleTypeface);
		buttonGallery.setTypeface(_titleTypeface);
		
		buttonPicture.setTextSize(12);
		buttonGallery.setTextSize(12);
		
		buttonPicture.setText("Take picture");
		buttonGallery.setText("Gallery");
		
		buttonPicture.setTag(mission.getId());
		buttonGallery.setTag(mission.getId());
		
		if (!User.hasCompleted(mission.getId())) {
			buttonPicture.setOnClickListener(new OnClickListener() {
	
				@Override
				public void onClick(View view) {
					int position = (Integer) view.getTag();
					_missionListOnClickListener.onTakePictureButtonClicked(position);
					
				}
				 
			});
			 
			buttonGallery.setOnClickListener(new OnClickListener() {
	
					@Override
					public void onClick(View view) {
						int position = (Integer) view.getTag();
						_missionListOnClickListener.onGalleryButtonClicked(position);
						
					}
					 
			});
		}

		
		return convertView;
	}
}
