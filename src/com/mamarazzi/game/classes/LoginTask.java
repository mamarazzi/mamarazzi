package com.mamarazzi.game.classes;


import org.json.JSONException;
import org.json.JSONObject;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.os.AsyncTask;
import android.util.Log;

public class LoginTask extends AsyncTask<Void, Boolean, AuthenticationResponse> {
	
	private String _login = "";
	private String _password = "";
	private LoginResponseListener _responseListener;
	
	public LoginTask(String login, String password) {
		_login = login;
		_password = password;
	}
	
	public void setLoginResponseListener(LoginResponseListener listener) {
		_responseListener = listener;
	}
	
	@Override
	protected AuthenticationResponse doInBackground(Void... arg0) {
		AuthenticationDatabaseManager handler = new AuthenticationDatabaseManager();
		AuthenticationResponse response;
		try {
			response = handler.tryLogin(_login, _password);
		} catch (MamarazziConnectionException e) {
			JSONObject jsonResponse = new JSONObject();
			try {
				jsonResponse.put("request", "login");
				jsonResponse.put("success", false);
				jsonResponse.put("error", 3);
				jsonResponse.put("error_message", "Could not connect to server.");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return new AuthenticationResponse(jsonResponse);
		}
		
		return response;
	}

	
	protected void onPostExecute(AuthenticationResponse result) {
		Log.d("Login:", "Result: " + result);
		_responseListener.onLoginResponse(result);
	}
	
	public interface LoginResponseListener {
		public void onLoginResponse(AuthenticationResponse response);
	}
	
}
