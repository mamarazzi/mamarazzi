package com.mamarazzi.game.classes;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetCommentsResponse {
	private static final String KEY_REQUEST = "request";
	private static final String KEY_SUCCESS = "success";
	private static final String KEY_PICTURE = "picture_id";
	private static final String KEY_COMMENTS = "comments";
	
	private String _request;
	private boolean _success;
	private int _picture;
	private ArrayList<Comment> _comments = new ArrayList<Comment>();
	
	public GetCommentsResponse(String response) {
		JSONObject json = null;
		try {
			json = new JSONObject(response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			_request = json.getString(KEY_REQUEST);
			_success = json.getBoolean(KEY_SUCCESS);
		
			if (_request.equals("get")) {
				if (_success) {
					_picture = json.getInt(KEY_PICTURE);
					JSONArray commentList = json.getJSONArray(KEY_COMMENTS);
					
					for (int i = 0; i < commentList.length(); i++) {
						Comment comment = new Comment(commentList.get(i).toString());
						_comments.add(comment);
					}
				}
				else {
					
				}
			}
			else {
				
			}
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public String getRequest() {
		return _request;
	}
	
	public boolean successful() {
		return _success;
	}
	
	public int getPictureId() {
		return _picture;
	}
	
	public ArrayList<Comment> getComments() {
		return _comments;
	}
}
