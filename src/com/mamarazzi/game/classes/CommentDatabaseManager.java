package com.mamarazzi.game.classes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import android.util.Log;
import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.ClientProtocolException;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.MultipartEntity;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

public class CommentDatabaseManager {
	
	public static final String COMMENT_URL = "http://mamarazzigame.com/comment_manager.php";
	
	public GetCommentsResponse addComment(int pictureId, int userId, String comment) throws MamarazziConnectionException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(COMMENT_URL);
		
		MultipartEntity httpEntity = new MultipartEntity();
		
		try {
			httpEntity.addPart("request", new StringBody("add"));
			httpEntity.addPart("picture_id", new StringBody(Integer.toString(pictureId)));
			httpEntity.addPart("user_id", new StringBody(Integer.toString(userId)));
			httpEntity.addPart("comment", new StringBody(comment));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		httpPost.setEntity(httpEntity);
		HttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new MamarazziConnectionException(e);
		}
		
		String responseString = buildResponseString(httpResponse);
		
		Log.d("Upload String: ", "" + responseString);
		
		GetCommentsResponse commentResponse = new GetCommentsResponse(responseString);
		
		return commentResponse;
	}
	
	public GetCommentsResponse getComments(int pictureId) throws MamarazziConnectionException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(COMMENT_URL);
		
		MultipartEntity httpEntity = new MultipartEntity();
		
		try {
			httpEntity.addPart("request", new StringBody("get"));
			httpEntity.addPart("picture_id", new StringBody(Integer.toString(pictureId)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		httpPost.setEntity(httpEntity);
		HttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new MamarazziConnectionException(e);
		}
		
		String responseString = buildResponseString(httpResponse);
		
		Log.d("Upload String: ", "" + responseString);
		
		GetCommentsResponse commentResponse = new GetCommentsResponse(responseString);
		
		return commentResponse;
	}
	
	
	private String buildResponseString(HttpResponse response) {
		HttpEntity responseEntity = response.getEntity();
		
		StringBuilder responseBuilder = new StringBuilder();
		try {
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
			String responseString = "";
			while ((responseString = responseReader.readLine()) != null) {
				responseBuilder.append(responseString);
			}
			responseReader.close();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return responseBuilder.toString();
	}
}
