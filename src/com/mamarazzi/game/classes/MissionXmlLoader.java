package com.mamarazzi.game.classes;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.util.Xml;


import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class MissionXmlLoader {
	
	private Context _context;
	private String _filename;
	private InputStream _stream;
	
	public MissionXmlLoader(Context context, String filename) {
		_context = context;
		_filename = filename;
	}
	
	public void setFilename(String filename) {
		_filename = filename;
	}
	
	public void open() {
		Log.d("open()", "Opening file.");
		AssetManager manager = _context.getAssets();
		try {
			_stream = manager.open(_filename);
		} catch (IOException e) {
			Log.d("open", "Failed to open file.");
			e.printStackTrace();
		}
	}
	
	public List<Mission> parse() {
		
		Log.d("parse()", "Starting parse.");
		List<Mission> missions = null;
		
		if (_stream == null) {
			Log.d("parse()", "_stream was null");
			throw new IllegalStateException();
		}
		
		
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(_stream, null);
			parser.nextTag();
			missions = readMissions(parser);
		}
		catch (XmlPullParserException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		Log.d("parse()", "End of parsing.");
		return missions;
	}
		
	
	private List<Mission> readMissions(XmlPullParser parser) throws IOException, XmlPullParserException {
		Log.d("readMissions()", "Starting mission reading.");
		
		List<Mission> missions = new ArrayList<Mission>();
		
		parser.require(XmlPullParser.START_TAG, null, "missions");
		while (parser.next() != XmlPullParser.END_TAG) {
			Log.d("readMissions()", "Beginning of while()");
			if (parser.getEventType() == XmlPullParser.START_TAG) {
				Log.d("readMissions()", "START_TAG found: " + parser.getName());
				if (parser.getName().equals("entry")) {
					missions.add(readMissionEntry(parser));
				}
			}
		}
		
		return missions;
	}
	
	private Mission readMissionEntry(XmlPullParser parser) throws IOException, XmlPullParserException {
		Log.d("readMissionEntry()", "Starting to read mission entry.");
		Mission newEntry = new Mission("");
		
		int missionId = 0;
		int missionLevel = 0;
		String missionTitle = "";
		
		
		parser.require(XmlPullParser.START_TAG, null, "entry");
		while(parser.nextTag() != XmlPullParser.END_TAG) {
			
			String name = parser.getName();
			
			if (name.equals("title")) {
				missionTitle = readMissionTitle(parser);
			}
			else if (name.equals("id")) {
				missionId = readMissionId(parser);
			}
			else if (name.equals("level")) {
				missionLevel = readMissionLevel(parser);
			}
			else {
				skipTag(parser);
			}
		}
		parser.require(XmlPullParser.END_TAG, null, "entry");
		
		Log.d("readMissionEntry", "New mission - Id = " + missionId + " Level = " + missionLevel + " Title = " + missionTitle);
		
		newEntry.setId(missionId);
		newEntry.setLevelRequirement(missionLevel);
		newEntry.setTitle(missionTitle);
		
		return newEntry;
	}
	
	
	
	
	private int readMissionId(XmlPullParser parser) throws IOException, XmlPullParserException{
		int id = -1;
		String text = "";
		
		parser.require(XmlPullParser.START_TAG, null, "id");
		int state = parser.next();
		if (state == XmlPullParser.TEXT) {
			text = parser.getText();
			id = Integer.parseInt(text);
		}
		parser.nextTag();
		parser.require(XmlPullParser.END_TAG, null, "id");
		
		return id;
	}
	
	private int readMissionLevel(XmlPullParser parser) throws IOException, XmlPullParserException {
		int level = -1;
		String text = "";
		
		parser.require(XmlPullParser.START_TAG, null, "level");
		int state = parser.next();
		if (state == XmlPullParser.TEXT) {
			text = parser.getText();
			level = Integer.parseInt(text);
		}
		parser.nextTag();
		parser.require(XmlPullParser.END_TAG, null, "level");
		
		return level;
	}
	
	private String readMissionTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
		String title = "";
		
		parser.require(XmlPullParser.START_TAG, null, "title");
		int state = parser.next();
		if (state == XmlPullParser.TEXT) {
			title = parser.getText();
		}
		parser.nextTag();
		parser.require(XmlPullParser.END_TAG, null, "title");
		
		return title;
	}
	
	private void skipTag(XmlPullParser parser) throws IOException, XmlPullParserException {
	  if (parser.getEventType() != XmlPullParser.START_TAG) {
		  	Log.d("skipTag", "Read a START_TAG when it shouldn't have.");
	        throw new IllegalStateException();
	    }
	    int depth = 1;
	    while (depth != 0) {
	        switch (parser.next()) {
	        case XmlPullParser.END_TAG:
	            depth--;
	            break;
	        case XmlPullParser.START_TAG:
	            depth++;
	            break;
	        }
	    } 
	}
}
