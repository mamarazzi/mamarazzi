package com.mamarazzi.game.classes;

import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSettingsManager {
	
	public static final String USER_SETTINGS_FILE = "userSettings";
	public static final String ID_KEY = "id";
	public static final String USERNAME_KEY = "username";
	public static final String EMAIL_KEY = "email";
	public static final String EXPERIENCE_KEY = "experience";
	public static final String COMPLETED_KEY = "completed";
	public static final String PROFILE_IMAGE_KEY = "profileImagePath";
	
	public static void setUserFromPreferences(Context context) {

		
		SharedPreferences preferences = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE);
		
		User.setUniqueId(preferences.getInt(ID_KEY, 0));
		User.setUsername(preferences.getString(USERNAME_KEY, "Guest"));
		User.setEmail(preferences.getString(EMAIL_KEY, "guest@guest.com"));
		User.setExperience(preferences.getInt(EXPERIENCE_KEY, 0));
		User.setProfileImageUrl(preferences.getString(PROFILE_IMAGE_KEY, "lolol"));
		User.setCompletion(preferences.getString(COMPLETED_KEY, "[]"));
		
	}
	
	
	
	/**
	 * Checks if a user is logged in.
	 * @param Application context.
	 * @return True if user is logged in. False if not.
	 */
	public static boolean isLoggedIn(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE);
		if (preferences.contains(ID_KEY)) {
			return true;
		}
		
		return false;
	}
	
	public static void setSettings(Context context, AuthenticationResponse response) {
		SharedPreferences.Editor editor = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		editor.putInt(ID_KEY, response.getId());
		editor.putString(USERNAME_KEY, response.getUsername());
		editor.putString(EMAIL_KEY, response.getEmail());
		editor.putInt(EXPERIENCE_KEY, response.getExperience());
		editor.putString(COMPLETED_KEY, response.getCompletion());
		editor.putString(PROFILE_IMAGE_KEY, response.getProfileImageUrl());
		editor.commit();
	}
	
	public static void setId(Context context, String id) {
		SharedPreferences.Editor editor = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		editor.putString(ID_KEY, id);
		editor.commit();
	}
	
	public static void setUsername(Context context, String username) {
		SharedPreferences.Editor editor = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		editor.putString(USERNAME_KEY, username);
		editor.commit();
	}
	
	public static void setEmail(Context context, String email) {
		SharedPreferences.Editor editor = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		editor.putString(EMAIL_KEY, email);
		editor.commit();
	}
	
	public static void setExperience(Context context, int experience) {
		SharedPreferences.Editor editor = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		editor.putInt(EXPERIENCE_KEY, experience);
		editor.commit();
	}
	
	public static void setCompleted(Context context, String completed) {
		SharedPreferences.Editor editor = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		editor.putString(COMPLETED_KEY, completed);
		editor.commit();
	}
	
	public static void setCompleted(Context context, List<Integer> completed) {
		JSONArray jsonArray = new JSONArray();
		for (Integer i : completed) {
			jsonArray.put(i);
		}
		
		setCompleted(context, jsonArray.toString());
		
	}
	
	public static void setProfileImagePath(Context context, String imagePath) {
		SharedPreferences.Editor editor = context.getSharedPreferences(USER_SETTINGS_FILE, Context.MODE_PRIVATE).edit();
		editor.putString(PROFILE_IMAGE_KEY, imagePath);
		editor.commit();
	}
	
}
