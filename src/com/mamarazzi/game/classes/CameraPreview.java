package com.mamarazzi.game.classes;



import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

	private Camera _camera = null;
	private SurfaceHolder _holder = null;
	private Camera.Size _camSize = null;
	private float _aspectRatio = 0;
	private List<Camera.Size> _cameraSizeList = null;
	
	@SuppressWarnings("deprecation")
	public CameraPreview(Context context) {
		super(context);
		
		// Retrieve holder from SurfaceView
		_holder = getHolder();
		Log.d("CameraPreview", "_holder = " + _holder);
		
		// Add callback for SurfaceHolder.Callback functions
		_holder.addCallback(this);
		
		// Some outdated stuff that apparently needs setting up for older versions of the Android OS
		_holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}


	public void setCamera(Camera camera) {
	
		// Checking if setting the camera i redundant. Can save some time
		if (_camera == camera)
			return;
		
		// Stop preview, release camera, set _camera to null.
		// Basically a hard reset.
		stopPreviewAndFreeCamera();
		
		_camera = camera;
		
		Log.d("setCamera", "Entry");
		Log.d("setCamera", "camera = " + camera);
		if (camera != null && _holder != null) {
			Log.d("setCamera", "camera and holder not null");
			
			Camera.Parameters params = _camera.getParameters();
			
			
			// Retrieve data regarding preview size and aspect ratio
			_cameraSizeList = params.getSupportedPreviewSizes();
			
			for (int i = 0; i < _cameraSizeList.size(); i++) {
				Log.d("_cameraSizeList", "Size: " + i);
				Log.d("_cameraSizeList", "Width: " + _cameraSizeList.get(i).width);
				Log.d("_cameraSizeList", "Height: " + _cameraSizeList.get(i).height);
			}
			
			params.setPreviewSize(_cameraSizeList.get(1).width, _cameraSizeList.get(1).height);
			_camSize = _camera.getParameters().getPreviewSize();
			
			_aspectRatio = (float) _camSize.width / (float) _camSize.height;			
			
			// Start the preview.
			_camera.setDisplayOrientation(90);
			_camera.setParameters(params);
			_camera.startPreview();
			
		}
	}
	
	public void surfaceChanged (SurfaceHolder holder, int format, int width, int height) {
		// If the surface changed, restart preview.
		// Calling startPreview while the preview is arleady running apparently resets it.
		_camera.startPreview();
	}

	
	// Still unused function. Don't know if I'll ever use it.
	@SuppressWarnings("unused")
	private void setSurfaceSize(int width, int height) {
		LayoutParams params = getLayoutParams();
		_aspectRatio = width / height;
		params.width = width;
		params.height = height;
		setLayoutParams(params);
	}
	
	// Called whenever the Surface in the SurfaceHolder of the SurfaceView is created.
	// Apprently it is recreated at every call of onCreate and onResume
	public void surfaceCreated (SurfaceHolder holder) {
		Log.d("surfaceCreated", "Entry");
		
		try {
			if (_camera != null) {
				// An obvious need of resetting the preview display.
				_camera.setPreviewDisplay(holder);
			}
		} 
		
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void surfaceDestroyed (SurfaceHolder holder) {
		Log.d("surfaceDestoryed", "Entry");
		if (_camera != null) {
			// If the surface is destroyed, we should stop previewing, as there is no surface to render the preview on.
			// Not sure if you should release the camera here.
			_camera.stopPreview();
		}
		
	}
	
	// Stops the preview and releases the camera. Also clears the private camera object.
	public void stopPreviewAndFreeCamera() {
		if (_camera != null) {
			_camera.stopPreview();
			_camera.release();
		
			_camera = null;
		}
	}
	

	@Override
	protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
		
		// Get maximum widh and height of the are for the SurfaceView to be laid out in.
		final int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
		final int maxHeight = MeasureSpec.getSize(heightMeasureSpec);
		
		int height = 0;
		int width = 0;
		
		Log.d("onMeasure", "Width = " + maxWidth);
		Log.d("onMeasure", "Height = " + maxHeight);
		
		// If the preview is wider than what is allowed,
		// Clamp by the height of the preview.
		if (_camSize.height >= maxHeight) {

			width = maxWidth;
			height =  (int) (maxWidth * _aspectRatio);
		}
		// If the preview is higher than what is allowed,
		// Clamp by the width of the preview.
		else if (_camSize.width >= maxWidth) {
			height = maxHeight;
			width = (int) (height / _aspectRatio);
		}
		// Else, just fill out everything.
		else {
			height = maxHeight;
			width = maxWidth;
			
		}
		Log.d("onMeasure", "_camSize.width" + _camSize.width);
		Log.d("onMeasure", "_camSize.height" + _camSize.height);
		Log.d("onMeasure", "Aspect ratio: " + _aspectRatio);
		Log.d("onMeasure", "Final width: " + width);
		Log.d("onMeasure", "Final height: " + height);
		
		setMeasuredDimension(width, height);
	}

}
