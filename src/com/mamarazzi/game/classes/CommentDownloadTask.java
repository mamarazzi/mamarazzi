package com.mamarazzi.game.classes;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.os.AsyncTask;
import android.util.Log;

public class CommentDownloadTask extends AsyncTask<Void, Void, GetCommentsResponse> {

	private OnDownloadCompleteListener _downloadListener = null;
	private static final String DEFAULT_RESPONSE = "{\"request\":\"get\",\"success\":false,\"comments\":[]}";
	private int _pictureId;
	
	public CommentDownloadTask(int pictureId) {
		_pictureId = pictureId;
	}

	@Override
	protected GetCommentsResponse doInBackground(Void... arg0) {
		CommentDatabaseManager manager = new CommentDatabaseManager();
		GetCommentsResponse response = null;
		try {
			response = manager.getComments(_pictureId);
		} catch (MamarazziConnectionException e) {
			e.printStackTrace();
			return new GetCommentsResponse(DEFAULT_RESPONSE);
		}
		return response;
	}
	
	protected void onPostExecute(GetCommentsResponse result) {
		Log.d("get comments:", "Result: " + result);
		_downloadListener.onDownloadComplete(result);
	}
	
	
	public void setOnDownloadCompleteListener(OnDownloadCompleteListener listener) {
		_downloadListener = listener;
	}
	
	public interface OnDownloadCompleteListener {
		public void onDownloadComplete(GetCommentsResponse response);
	}

}
