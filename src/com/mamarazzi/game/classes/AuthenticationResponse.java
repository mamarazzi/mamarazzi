package com.mamarazzi.game.classes;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class AuthenticationResponse {

	private static final String REQUEST_KEY = "request";
	private static final String SUCCESS_KEY = "success";
	private static final String ERROR_KEY = "error";
	private static final String ERRORMESSAGE_KEY = "error_message";
	private static final String SESSION_FILE = "session_data";
	private static final String USERNAME_KEY = "username";
	private static final String EMAIL_KEY = "email";
	private static final String ID_KEY = "id";
	private static final String COMPLETED_KEY = "completed";
	private static final String EXPERIENCE_KEY = "experience";
	private static final String PROFILEIMAGE_KEY = "profile_image";
	private static final String USERDETAILS_KEY = "user_details";
	
	private String _request = "";
	private boolean _success = true;
	private int _error = 0;
	private String _errorMessage;
	private String _username = "";
	private String _email = "";
	private int _id = -1;
	private int _experience = -1;
	private String _completion = "";
	private String _profileImageUrl = "";
	
	
	public AuthenticationResponse(String responseString) {
		try {
			JSONObject json = new JSONObject(responseString);
			
			_request = json.getString(REQUEST_KEY);
			if (_request.equals("login")) {
				_success = json.getBoolean(SUCCESS_KEY);
				
				if (!_success) {
					_error = json.getInt(ERROR_KEY);
					_errorMessage = json.getString(ERRORMESSAGE_KEY);
				}
				else if (_success) {
					_username = json.getJSONObject(USERDETAILS_KEY).getString(USERNAME_KEY);
					_email = json.getJSONObject(USERDETAILS_KEY).getString(EMAIL_KEY);
					_id = json.getJSONObject(USERDETAILS_KEY).getInt(ID_KEY);
					_experience = json.getJSONObject(USERDETAILS_KEY).getInt(EXPERIENCE_KEY);
					_completion = json.getJSONObject(USERDETAILS_KEY).getString(COMPLETED_KEY);
					_profileImageUrl = json.getJSONObject(USERDETAILS_KEY).getString(PROFILEIMAGE_KEY);
				}
			}
			else if (_request.equals("register")) {
				Log.d("AuthenticationResponse", "register requests not implemented yet");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public AuthenticationResponse(JSONObject responseObject) {
		try {
			_request = responseObject.getString(REQUEST_KEY);
			if (_request.equals("login")) {
				_success = responseObject.getBoolean(SUCCESS_KEY);
				
				if (!_success) {
					_error = responseObject.getInt(ERROR_KEY);
					_errorMessage = responseObject.getString(ERRORMESSAGE_KEY);
				}
				else if (_success) {
					_username = responseObject.getJSONObject(USERDETAILS_KEY).getString(USERNAME_KEY);
					_email = responseObject.getJSONObject(USERDETAILS_KEY).getString(EMAIL_KEY);
					_id = responseObject.getJSONObject(USERDETAILS_KEY).getInt(ID_KEY);
					_experience = responseObject.getJSONObject(USERDETAILS_KEY).getInt(EXPERIENCE_KEY);
					_completion = responseObject.getJSONObject(USERDETAILS_KEY).getString(COMPLETED_KEY);
					_profileImageUrl = responseObject.getJSONObject(USERDETAILS_KEY).getString(PROFILEIMAGE_KEY);
				}
			}
			else if (_request.equals("register")) {
				Log.d("AuthenticationResponse", "register requests not implemented yet");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void setRequest(String request) {
		_request = request;
	}
	
	public void setSuccess(boolean success) {
		_success = success;
	}
	
	public void setError(int error) {
		_error = error;
	}
	
	public void setErrorMessage(String errorMessage) {
		_errorMessage = errorMessage;
	}

	public void setUsername(String username) {
		_username = username;
	}

	public void setEmail(String email) {
		_email = email;
	}
	
	public void setUniqueId(int id) {
		_id = id;
	}
	
	public void setExperience(int experience) {
		_experience = experience;
	}
	
	public void setCompletion(String completion) {
		_completion = completion;
	}
	
	public void setProfileImageUrl(String profileImageUrl) {
		_profileImageUrl = profileImageUrl;
	}
	
	public String getRequest() {
		return _request;
	}
	
	public boolean successful() {
		return _success;
	}
	
	public int getError() {
		return _error;
	}
	
	public String getErrorMessage() {
		return _errorMessage;
	}
	
	public String getUsername() {
		return _username;
	}
	
	public String getEmail() {
		return _email;
	}
	
	public int getId() {
		return _id;
	}
	
	public int getExperience() {
		return _experience;
	}
	
	public String getCompletion() {
		return _completion;
	}
	
	public String getProfileImageUrl() {
		return _profileImageUrl;
	}
}
