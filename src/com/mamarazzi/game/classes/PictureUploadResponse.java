package com.mamarazzi.game.classes;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class PictureUploadResponse {

	
	private static final String KEY_REQUEST = "request";
	private static final String KEY_SUCCESS = "success";
	private static final String KEY_ERROR = "error";
	private static final String KEY_ERRORMESSAGE = "error_message";
	private static final String KEY_PICTUREID = "picture_id";
	
	private String _request = "";
	private boolean _success = false;
	private int _error = 0;
	private String _errorMessage = "";
	private int _pictureId = 0;
	
	public PictureUploadResponse() {
		
	}
	
	public PictureUploadResponse(String response) {
		try {
			Log.d("PictureUploadResponse", "response: " + response);
			JSONObject json = new JSONObject(response);
			_request = json.getString(KEY_REQUEST);
			
			if (_request.equals("upload")) {
				_success = json.getBoolean(KEY_SUCCESS);
				
				
				if (!_success) {
					_error = json.getInt(KEY_ERROR);
					_errorMessage = json.getString(KEY_ERRORMESSAGE);
				}
				else {
					_pictureId = json.getInt(KEY_PICTUREID);
				}
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public void setRequest(String request) {
		_request = request;
	}
	
	public String getRequest() {
		return _request;
	}
	
	public void setSuccess(boolean success) {
		_success = success;
	}
	
	public boolean successful() {
		return _success;
	}
	
	public void setError(int error) {
		_error = error;
	}
	
	public int getError() {
		return _error;
	}
	
	public void setErrorMessage(String errorMessage) {
		_errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return _errorMessage;
	}
	
	public int getPictureId() {
		return _pictureId;
	}
}
