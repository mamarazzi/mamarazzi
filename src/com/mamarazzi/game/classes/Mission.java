package com.mamarazzi.game.classes;


public class Mission {

	private String _title = "Take a picture!";
	private int _levelRequirement = 0;
	private int _id = 0;
	private int _type = 0;
	
	public Mission() {
	}
	
	public Mission(String title) {
		_title = title;
	}
	
	public int getId() {
		return _id;
	}
	
	public void setId(int id) {
		_id = id;
	}
	
	public String getTitle() {
		return _title;
	}
	
	public void setTitle(String title) {
		_title = title;
	}
	
	public int getType() {
		return _type;
	}
	
	public void setType(int type) {
		_type = type;
	}
	
	public int getLevelRequirement() {
		return _levelRequirement;
	}
	
	public void setLevelRequirement(int levelRequirement) {
		_levelRequirement = levelRequirement;
	}
	

}
