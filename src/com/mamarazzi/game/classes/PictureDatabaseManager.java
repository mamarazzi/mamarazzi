package com.mamarazzi.game.classes;

import java.io.BufferedReader;

import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.ClientProtocolException;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpGet;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.FileEntity;
import ch.boye.httpclientandroidlib.entity.mime.MultipartEntity;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;


import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.util.Log;

public class PictureDatabaseManager {

	
	public static final String UPLOAD_URL = "http://mamarazzigame.com/image_upload.php";
	
	public PictureUploadResponse tryUpload(File file, int userId, int missionId) throws MamarazziConnectionException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(UPLOAD_URL);
		
		MultipartEntity httpEntity = new MultipartEntity();
		
		try {
			httpEntity.addPart("request", new StringBody("upload"));
			httpEntity.addPart("author_id", new StringBody(Integer.toString(userId)));
			httpEntity.addPart("mission_id", new StringBody(Integer.toString(missionId)));
			httpEntity.addPart("picture", new FileBody(file));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		httpPost.setEntity(httpEntity);
		HttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new MamarazziConnectionException(e);
		}
		
		String responseString = buildResponseString(httpResponse);
		
		Log.d("Upload String: ", "" + responseString);
		
		PictureUploadResponse pictureResponse = new PictureUploadResponse(responseString);
		
		return pictureResponse;
	}
	
	public PictureMetaDownloadResponse getPictureMeta() throws MamarazziConnectionException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(UPLOAD_URL);
		
		MultipartEntity httpEntity = new MultipartEntity();
		
		try {
			httpEntity.addPart("request", new StringBody("get"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		httpPost.setEntity(httpEntity);
		HttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new MamarazziConnectionException(e);
		}
		
		String responseString = buildResponseString(httpResponse);
		
		Log.d("Get String: ", "" + responseString);
		
		PictureMetaDownloadResponse pictureResponse = new PictureMetaDownloadResponse(responseString);
		
		return pictureResponse;
	}

	public InputStream getPicture(String url) throws MamarazziConnectionException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		HttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(httpGet);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new MamarazziConnectionException(e);
		}
		
		FileEntity fileEntity = (FileEntity) httpResponse.getEntity();
		
		InputStream inStream = null;
		try {
			inStream = fileEntity.getContent();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return inStream;
	}
	
	private String buildResponseString(HttpResponse response) {
		HttpEntity responseEntity = response.getEntity();
		
		StringBuilder responseBuilder = new StringBuilder();
		try {
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
			String responseString = "";
			while ((responseString = responseReader.readLine()) != null) {
				responseBuilder.append(responseString);
			}
			responseReader.close();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return responseBuilder.toString();
	}
}
