package com.mamarazzi.game.classes;

import java.io.InputStream;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.os.AsyncTask;

public class PictureDownloadTask extends AsyncTask<String, Void, InputStream> {

	public OnPictureDownloadCompleteListener _downloadListener = null;
	
	public PictureDownloadTask() {
		
	}
	
	public void setOnPictureDownloadCompleteListener(OnPictureDownloadCompleteListener listener) {
		_downloadListener = listener;
	}
	
	@Override
	protected InputStream doInBackground(String... url) {
		PictureDatabaseManager manager = new PictureDatabaseManager();
		
		InputStream inStream = null;
		try {
			inStream = manager.getPicture(url[0]);
		} catch (MamarazziConnectionException e) {
			return null;
		}
		
		return inStream;
	}
	
	@Override
	protected void onPostExecute(InputStream result) {
		_downloadListener.onPictureDownloaded(result);
	}
	
	public interface OnPictureDownloadCompleteListener {
		public void onPictureDownloaded(InputStream inStream);
	}
	
}
