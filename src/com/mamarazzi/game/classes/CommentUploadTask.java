package com.mamarazzi.game.classes;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.os.AsyncTask;
import android.util.Log;

public class CommentUploadTask extends AsyncTask<Void, Void, GetCommentsResponse> {

	private OnUploadCompleteListener _uploadListener = null;
	private static final String DEFAULT_RESPONSE = "{\"request\":\"add\",\"success\":false}";
	private int _pictureId;
	private int _userId;
	private String _comment;
	
	public CommentUploadTask(int pictureId, int userId, String comment) {
		_pictureId = pictureId;
		_userId = userId;
		_comment = comment;
	}

	@Override
	protected GetCommentsResponse doInBackground(Void... arg0) {
		CommentDatabaseManager manager = new CommentDatabaseManager();
		GetCommentsResponse response = null;
		try {
			response = manager.addComment(_pictureId, _userId, _comment);
		} catch (MamarazziConnectionException e) {
			e.printStackTrace();
			return new GetCommentsResponse(DEFAULT_RESPONSE);
		}
		return response;
	}
	
	protected void onPostExecute(GetCommentsResponse result) {
		Log.d("get comments:", "Result: " + result);
		if (_uploadListener != null) 
			_uploadListener.onUploadComplete(result);
	}
	
	
	public void setOnUploadCompleteListener(OnUploadCompleteListener listener) {
		_uploadListener = listener;
	}
	
	public interface OnUploadCompleteListener {
		public void onUploadComplete(GetCommentsResponse response);
	}

}
