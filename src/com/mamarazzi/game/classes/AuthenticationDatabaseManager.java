package com.mamarazzi.game.classes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.ClientProtocolException;
import ch.boye.httpclientandroidlib.client.entity.UrlEncodedFormEntity;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import ch.boye.httpclientandroidlib.message.BasicNameValuePair;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

public class AuthenticationDatabaseManager {
	
	public static final String AUTH_URL = "http://www.mamarazzigame.com/user_verification.php";

	
	public AuthenticationDatabaseManager() {
		
	}
	
	private String requestForm(ArrayList<BasicNameValuePair> formData) throws MamarazziConnectionException {
		
		HttpResponse postResponse = null;
		DefaultHttpClient httpClient = null;
		HttpPost postRequest = null;
		try {
			
			URI loginURL = new URI(AUTH_URL);
			Log.d("AuthentiationDatabaseManager.requestForm", "loginURL: " + loginURL.toString());
			httpClient = new DefaultHttpClient();
			postRequest = new HttpPost(loginURL);
			postRequest.setEntity(new UrlEncodedFormEntity(formData));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		try {
			postResponse = httpClient.execute(postRequest);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new MamarazziConnectionException(e);
		}
		
	
		return buildResponseString(postResponse);
	}
	
	private String buildResponseString(HttpResponse response) {
		HttpEntity responseEntity = response.getEntity();
		
		StringBuilder responseBuilder = new StringBuilder();
		try {
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
			String responseString = "";
			while ((responseString = responseReader.readLine()) != null) {
				responseBuilder.append(responseString);
			}
			// responseReader.close();
			
		} catch (IllegalStateException e) {
			Log.d("Message: ", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("Message: ", e.getMessage());
			e.printStackTrace();
		}
		
		Log.d("buildResponseString", "ending");
		return responseBuilder.toString();
	}
	
	public AuthenticationResponse tryLogin(String login, String password) throws MamarazziConnectionException {
		
		ArrayList<BasicNameValuePair> nameValueArray = new ArrayList<BasicNameValuePair>();
		nameValueArray.add(new BasicNameValuePair("request", "login"));
		nameValueArray.add(new BasicNameValuePair("login", login));
		nameValueArray.add(new BasicNameValuePair("password", password));
		
		JSONObject json;
		AuthenticationResponse response = null;
		try {
			String responseString = requestForm(nameValueArray);
			Log.d("tryLogin", "responseString: " + responseString);
			json = new JSONObject(responseString);

			response = new AuthenticationResponse(json);
			
			Log.d("TryLogin", "success: " + response.successful());
			Log.d("TryLogin", "error: " + response.getError());
			Log.d("TryLogin", "error_message: " + response.getErrorMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return response;
	}
}
