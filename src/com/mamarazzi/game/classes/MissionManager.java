package com.mamarazzi.game.classes;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.util.Log;

public class MissionManager implements MissionDownloader.MissionDownloadListener {
	
	private static final String MISSION_URL = "http://www.mamarazzigame.com/mission_manager2.php";
	
	private MissionSQLHelper _helper = null;
	private SQLiteDatabase _database = null;
	private MissionDownloader _downloader = null;
	private List<MissionDatabaseListener> _listeners = null;
	
	public MissionManager(Context context) {
		_helper = new MissionSQLHelper(context);
		_listeners = new ArrayList<MissionDatabaseListener>();
		_downloader = new MissionDownloader();
		_downloader.setMissionDownloadListener(this);
	}
	
	public void open() {
		_database = _helper.getWritableDatabase();
		_downloader.setExclusionList(getMissionIdList());
	}
	
	public void close() {
		_helper.close();
	}
	
	
	
	public void requestUpdate() {
		try {
			_downloader.execute(new URI(MISSION_URL));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	public Mission getMission(int id) {
		Cursor cursor = getMissionCursor(id);
		if (!cursor.moveToFirst())
			return null;
		Mission mission = new Mission();
		mission.setId(cursor.getInt(cursor.getColumnIndex(MissionSQLHelper.COLUMN_ID)));
		mission.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(MissionSQLHelper.COLUMN_TITLE)));
		mission.setLevelRequirement(cursor.getInt(cursor.getColumnIndex(MissionSQLHelper.COLUMN_LEVEL)));
		mission.setType(cursor.getInt(cursor.getColumnIndex(MissionSQLHelper.COLUMN_TYPE)));
		
		return mission;
	}
	
	/**
	 * Builds and returns a list of the available missions with respect to specified levels and types.
	 * @param level Level of missions to be retrieved.
	 * @param type Type of missions to be retrieved.
	 * @return A list of available missions with respect to specified levels and types.
	 */
	public ArrayList<Mission> getMissionList(int level, int type) {
		Cursor cursor = getMissionsCursor(level, type);
		ArrayList<Mission> missionList = new ArrayList<Mission>();
		
		if (cursor.moveToFirst() == false) {
			cursor.close();
			return missionList;
		}
		
		while (!cursor.isAfterLast()) {
			Mission mission = new Mission();
			mission.setId(cursor.getInt(cursor.getColumnIndex(MissionSQLHelper.COLUMN_ID)));
			mission.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(MissionSQLHelper.COLUMN_TITLE)));
			mission.setLevelRequirement(cursor.getInt(cursor.getColumnIndex(MissionSQLHelper.COLUMN_LEVEL)));
			mission.setType(cursor.getInt(cursor.getColumnIndex(MissionSQLHelper.COLUMN_TYPE)));
			
			missionList.add(mission);
			cursor.moveToNext();
		}
		cursor.close();
		
		return missionList;
	}
	
	/**
	 * Returns a list of IDs loaded in the database. 
	 * Useful for comparing with the online mission database to
	 * determine what missions should be downloaded.
	 * This can be used to reduce server traffic.
	 * @return A list of IDs loaded in the database.
	 */
	private ArrayList<Integer> getMissionIdList() {
		Log.d("getMisisonIdList", "Begin!");
		Cursor cursor = getMissionIdsCursor();
		ArrayList<Integer> idList = new ArrayList<Integer>();
		
		if (!cursor.moveToFirst()) {
			cursor.close();
			return idList;
		}
		
		while (!cursor.isAfterLast()) {
			Log.d("cursor.getInt(0)", "" + cursor.getInt(0));
			idList.add(cursor.getInt(0));
			cursor.moveToNext();
		}
		
		cursor.close();
		
		return idList;
	}
	
	/**
	 * Adds a listener to listen to database changes.
	 * @param listener Listener to add.
	 */
	public void addMissionDatabaseListener(MissionDatabaseListener listener) {
		_listeners.add(listener);
	}
	
	/**
	 * Notifies all MissionDatabaseListeners of changes that have been made to the database.
	 */
	private void notifyMissionDatabaseListeners() {
		for (MissionDatabaseListener listener : _listeners) {
			listener.onMissionDatabaseChanged();
		}
	}
	
	/**
	 * Inserts a mission into the SQL database.
	 * Presumes you have an open database to fetch from.
	 * @param database The database object.
	 * @param missionId The mission id.
	 * @param title The title text of the mission.
	 * @param type The type number of the mission.
	 * @param level The level requirement of the mission.
	 * @return The index of the inserted mission.
	 */
	public long insertMission(int id, String title, int type, int level) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(MissionSQLHelper.COLUMN_ID, id);
		contentValues.put(MissionSQLHelper.COLUMN_TITLE, title);
		contentValues.put(MissionSQLHelper.COLUMN_TYPE, type);
		contentValues.put(MissionSQLHelper.COLUMN_LEVEL, level);
		
		long rowIndex = _database.insert(MissionSQLHelper.TABLE_NAME, null, contentValues);
		
		return rowIndex;
	}
	
	/**
	 * Inserts mission into the SQL database.
	 * Opens a fresh instance of the database.
	 * @param mission
	 * @return The row index of the inserted object. -1 if error occured.
	 */
	public long insertMission(Mission mission) {
		long rowIndex = insertMission(
				mission.getId(), 
				mission.getTitle(), 
				mission.getType(), 
				mission.getLevelRequirement());
		return rowIndex;
	}
	
	
	/**
	 * Inserts list of missions into the SQL database.
	 * Presumes you have opened the database.
	 * @param database
	 * @param missionList
	 * @return The number of errors that occured. Most commonly due to index collision.
	 */
	public int insertMissions(List<Mission> missionList) {
		int errors = 0;
		for (Mission mission : missionList) {
			if (insertMission(mission) == -1)
				errors++;
		}
		
		return errors;
	}
	
	/**
	 * Returns a cursor to before the Mission entry with the requested ID.
	 * @param id Requested ID.
	 * @return A cursor to before the Mission entry with the requested ID.
	 */
	private Cursor getMissionCursor(int id) {
		Cursor cursor = _database.query(
				MissionSQLHelper.TABLE_NAME,
				null,
				null,
				null,
				null,
				null,
				null);
		return cursor;
	}
	

	/**
	 * Gets Cursor to list of IDs of all available missions.
	 * @return A cursor pointing to before the first element of the list.
	 */
	private Cursor getMissionIdsCursor() {
		Log.d("getMissionIds", "Begin. _database: " + _database);
		Cursor cursor = _database.query(
				MissionSQLHelper.TABLE_NAME, 
				new String[] {MissionSQLHelper.COLUMN_ID}, 
				null, 
				null, 
				null, 
				null, 
				MissionSQLHelper.COLUMN_ID + " ASC");
		Log.d("getMissionIds", "cursor: " + cursor);
		return cursor;
	}
	
	/**
	 * Gets Cursor to list of missions of specified level and type.
	 * @param level Specified level to get. A value of -1 makes query ignore level.
	 * @param type Specified type to get. A value of -1 makes query ignore type.
	 * @return A Cursor to before first element of the list.
	 */
	private Cursor getMissionsCursor(int level, int type) {
		String whereStatement = null;
		
		if (level > 0 && type > 0) {
			whereStatement = MissionSQLHelper.WHERE_LEVEL + level + " AND " + MissionSQLHelper.WHERE_TYPE + type;
		}
		else if (level > 0) {
			whereStatement = MissionSQLHelper.WHERE_LEVEL + level;
		}
		else if (type > 0) {
			whereStatement = MissionSQLHelper.WHERE_TYPE + type;
		}
		
		Cursor cursor = _database.query(MissionSQLHelper.TABLE_NAME, null, whereStatement, null, null, null, MissionSQLHelper.ORDERBY_TYPE_LEVEL);
		
		return cursor;
	}
	
	/**
	 * Public interface for items listening to the mission database's various changes.
	 * @author Dawlight
	 *
	 */
	
	public interface MissionDatabaseListener {
		public void onMissionDatabaseChanged();
	}

	private int insertResponse(MissionDownloader.Response response) {
		return insertMissions(response.getMissions());
	}
	
	@Override
	public void onMissionDownloadComplete(MissionDownloader.Response response) {
		if (response.successful() == 1) {
			int errors = insertResponse(response);
			notifyMissionDatabaseListeners();
		
			Log.d("Errors", "Erros: " + errors);
		}
	}
}
