package com.mamarazzi.game.classes;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MissionSQLHelper extends SQLiteOpenHelper {

	public static final String TABLE_NAME = "missions";
	
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_LEVEL = "level";
	
	public static final String WHERE_TYPE =  COLUMN_TYPE + " = ";
	public static final String WHERE_LEVEL = COLUMN_LEVEL + " = ";
	
	public static final String ORDERBY_TYPE_LEVEL = "'" + COLUMN_TYPE + "', '" + COLUMN_LEVEL + "'";
	
	public static final int TABLE_VERSION = 1;
	
	public final String CREATE_DATABASE = 
			"CREATE TABLE " + 
			TABLE_NAME +
			" (" + COLUMN_ID + " INTEGER PRIMARY KEY, " +
			COLUMN_TITLE + " TEXT NOT NULL, " +
			COLUMN_TYPE + " INTEGER NOT NULL, " +
			COLUMN_LEVEL + " INTEGER NOT NULL);";
			
	
	public MissionSQLHelper(Context context) {
		super(context, TABLE_NAME, null, TABLE_VERSION);
		
		Log.d("CREATE_DATABASE:", "" + CREATE_DATABASE);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_DATABASE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(db);
		
	}

}
