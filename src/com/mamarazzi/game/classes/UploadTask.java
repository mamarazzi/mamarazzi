package com.mamarazzi.game.classes;

import java.io.File;

import com.mamarazzi.game.exceptions.MamarazziConnectionException;

import android.os.AsyncTask;
import android.util.Log;

public class UploadTask extends AsyncTask<Void, Void, PictureUploadResponse> {

	private File _file = null;
	private int _userId = -1;
	private int _missionId = -1;
	private PictureUploadListener _uploadListener = null;
	
	public UploadTask(File file, int userId, int missionId) {
		_file = file;
		_userId = userId;
		_missionId = missionId;
	}
	
	@Override
	protected PictureUploadResponse doInBackground(Void... params) {
		PictureDatabaseManager manager = new PictureDatabaseManager();
		
		PictureUploadResponse response = null;
		try {
			response = manager.tryUpload(_file, _userId, _missionId);
		} catch (MamarazziConnectionException e) {
			
			Log.d("Exception Caught", "MamarazziConnectionException");
			response = new PictureUploadResponse();
			response.setRequest("upload");
			response.setSuccess(false);
			response.setError(3);
			response.setErrorMessage("Could not connect to server");
			
			return response;
		}
		
		return response;
	}
	
	protected void onPostExecute(PictureUploadResponse result) {
		Log.d("Upload", "Result: " + result.getError());
		
		_uploadListener.onPictureUploadComplete(result);
	}
	
	public void setPictureUploadListener(PictureUploadListener listener) {
		_uploadListener = listener;
	}
	
	
	public interface PictureUploadListener {
		public void onPictureUploadComplete(PictureUploadResponse response);
	}

	

}
