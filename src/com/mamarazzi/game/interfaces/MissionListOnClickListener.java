package com.mamarazzi.game.interfaces;

public interface MissionListOnClickListener {
	
	void onTakePictureButtonClicked(int id);
	
	void onGalleryButtonClicked(int id);
	
}
