package com.mamarazzi.game.activities;

import java.util.ArrayList;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MenuPagerAdapter extends FragmentPagerAdapter {
	
	FragmentManager _fragmentManager;
	int _fragmentCount = 0;
	ArrayList<Fragment> _fragments = new ArrayList<Fragment>();
	
	public MenuPagerAdapter(FragmentManager fm) {
		super(fm);
		_fragmentManager = fm;
	}
 
	public void addFragment(Fragment fragment) {
		_fragments.add(fragment);
	}
	
	@Override
	public Fragment getItem(int index) {
		return _fragments.get(index);
	}

	@Override
	public int getCount() {
		return _fragments.size();
	}

}
