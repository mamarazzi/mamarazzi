package com.mamarazzi.game.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mamarazzi.game.R;
import com.mamarazzi.game.classes.AuthenticationResponse;
import com.mamarazzi.game.classes.LoginTask;
import com.mamarazzi.game.classes.UserSettingsManager;

public class LoginActivity extends Activity implements LoginTask.LoginResponseListener {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		if (UserSettingsManager.isLoggedIn(this)) {
			login();
			finish();
			return;
		}

		setContentView(R.layout.activity_login);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void tryLogin(View view) {
		
		// Find EditText widgets.
		EditText loginText = (EditText) this.findViewById(R.id.editText1);
		EditText passwordText = (EditText) this.findViewById(R.id.editText2);
		
		// Hide login button to prevent spamming. Temporary solution.
		Button loginButton = (Button) this.findViewById(R.id.loginButton);
		Button registerButton = (Button) this.findViewById(R.id.registerButton);
		loginButton.setVisibility(View.INVISIBLE);
		registerButton.setVisibility(View.INVISIBLE);
		
		
		// Retrieve current content of EditText widgets.
		String loginString = loginText.getText().toString();
		String passwordString = passwordText.getText().toString();
		
		Log.d("loginText", loginString);
		Log.d("passText", passwordString);
		
		
		// Start asynchronous login task.
		// When done, onLoginResponse is called.
		LoginTask loginTask = new LoginTask(loginString, passwordString);
		loginTask.setLoginResponseListener(this);
		loginTask.execute();
	}
	
	// Start MainActivity.
	public void login() {
		Intent loginIntent = new Intent(this, MainActivity.class);
		this.startActivity(loginIntent);
	}

	// Retrieves the response of the asynchronous login task.
	// If response is successful, Toast! and switch to MainActivity.
	@Override
	public void onLoginResponse(AuthenticationResponse response) {
		Log.d("response:", "Response: " + response.getCompletion());
		if (response.successful()) {
			UserSettingsManager.setSettings(this, response);
			Toast toast = Toast.makeText(this, "Logged in as " + response.getUsername(), Toast.LENGTH_LONG);
			toast.show();
			login();
		}
		else {
			Toast toast = Toast.makeText(this, response.getErrorMessage(), Toast.LENGTH_LONG);
			toast.show();
		}
		Button loginButton = (Button) this.findViewById(R.id.loginButton);
		Button registerButton = (Button) this.findViewById(R.id.registerButton);
		loginButton.setVisibility(View.VISIBLE);
		registerButton.setVisibility(View.VISIBLE);
	}
	
	
}
