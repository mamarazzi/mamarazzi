package com.mamarazzi.game.activities;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.mamarazzi.game.CommunityFragment;
import com.mamarazzi.game.MissionFragment;
import com.mamarazzi.game.ProfileFragment;
import com.mamarazzi.game.R;
import com.mamarazzi.game.UnlocksFragment;
import com.mamarazzi.game.classes.MissionManager;
import com.mamarazzi.game.classes.User;
import com.mamarazzi.game.classes.UserSettingsManager;
import com.mamarazzi.game.interfaces.MissionListOnClickListener;
import com.mamarazzi.game.views.StaticViewPager;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

@SuppressLint("SimpleDateFormat")
public class MainActivity extends FragmentActivity implements
		MissionListOnClickListener {

	private Uri _pictureFileUri = null;
	private int _progress = 0;
	private int _clickedMissionId = 0;

	public final static String INSTANCESTATE_PICTUREFILEURI = "pictureFileUri";
	
	public final static String MISSION_ID = "com.mamarazzi.game.MISSION_ID";
	public final static String MISSION_FILENAME = "missions.json";
	private final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private final static int UPLOAD_IMAGE_ACTIVITY_REQUEST_CODE = 200;

	private StaticViewPager _fragmentViewPager = null;
	private MenuPagerAdapter _fragmentAdapter = null;

	private MissionFragment _missionFragment = new MissionFragment();
	private ProfileFragment _profileFragment = new ProfileFragment();
	private CommunityFragment _communityFragment = new CommunityFragment();
	private UnlocksFragment _unlocksFragment = new UnlocksFragment();

	private MenuButtonManager _buttonManager = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		_fragmentAdapter = new MenuPagerAdapter(getSupportFragmentManager());
		_fragmentAdapter.addFragment(_unlocksFragment);
		_fragmentAdapter.addFragment(_communityFragment);
		_fragmentAdapter.addFragment(_missionFragment);
		_fragmentAdapter.addFragment(_profileFragment);

		_fragmentViewPager = (StaticViewPager) findViewById(R.id.mainFragmentContainer);
		_fragmentViewPager.setAdapter(_fragmentAdapter);

		// Here begins actual cool code.

		 UserSettingsManager.setUserFromPreferences(this);

		_buttonManager = new MenuButtonManager(this);
		_buttonManager.setSelection(0);

		TextView missionScreenTitle = (TextView) findViewById(R.id.screenTitle);
		missionScreenTitle.setText(R.string.screen_title_profile);

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
	        .cacheOnDisc()
	        .build();
		ImageLoaderConfiguration imageLoaderConfig = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				    .defaultDisplayImageOptions(defaultOptions)
				    .enableLogging()
					.build();
		ImageLoader.getInstance().init(imageLoaderConfig);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				Log.d("onActivityResult", "Intent data: " + data);
				Log.d("onActivityResult", "_pictureFileUri: " + _pictureFileUri);
				Log.d("onActivityResult", "RESULT_OK");
				
				Bundle uploadBundle = new Bundle();
				uploadBundle.putInt("mission_id", _clickedMissionId);
				Intent uploadIntent = new Intent(this,
						PictureUploadActivity.class);
				uploadIntent.putExtras(uploadBundle);
				uploadIntent.putExtra(PictureUploadActivity.UPLOAD_URI,
						_pictureFileUri.getPath());
				startActivityForResult(uploadIntent,
						UPLOAD_IMAGE_ACTIVITY_REQUEST_CODE);
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Log.d("onActivityResult", "RESULT_CANCELED");
			} else if (resultCode == Activity.RESULT_FIRST_USER) {
				Log.d("onActivityResult", "RESULT_FIRST_USER ");
			}
		} else if (requestCode == UPLOAD_IMAGE_ACTIVITY_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				Bundle dataBundle = data.getExtras();
				int completedMission = dataBundle.getInt("mission_id");
				finishMission(completedMission);

				LayoutInflater inflater = getLayoutInflater();
				View toastView = inflater.inflate(
						R.layout.toast_mission_complete,
						(ViewGroup) findViewById(R.id.toastMissionComplete));
				Toast toast = new Toast(getApplicationContext());
				toast.setView(toastView);
				toast.setGravity(Gravity.CENTER_HORIZONTAL
						| Gravity.CENTER_VERTICAL, 0, 0);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.show();
			} else if (resultCode == Activity.RESULT_CANCELED) {
				LayoutInflater inflater = getLayoutInflater();
				View toastView = inflater.inflate(
						R.layout.toast_mission_canceled,
						(ViewGroup) findViewById(R.id.toastMissionCanceled));
				Toast toast = new Toast(getApplicationContext());
				toast.setView(toastView);
				toast.setGravity(Gravity.CENTER_HORIZONTAL
						| Gravity.CENTER_VERTICAL, 0, 0);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.show();
			}
		}
	}

	@Override
	protected void onSaveInstanceState (Bundle outState) {
		if (_pictureFileUri != null) 
			outState.putString(INSTANCESTATE_PICTUREFILEURI, _pictureFileUri.toString());
	}
	
	@Override
	protected void onRestoreInstanceState (Bundle savedInstanceState) {
		_pictureFileUri = Uri.parse(savedInstanceState.getString(INSTANCESTATE_PICTUREFILEURI));
	}
	
	public MissionManager getMissionManager() {
		return _missionFragment.getMissionManager();
	}
	
	public void finishMission(int id) {
		User.addCompletedMission(id);
		UserSettingsManager.setCompleted(this, User.getCompletion());
		Log.d("finishMission", "_missionFragment: " + _missionFragment);
		_missionFragment.refreshMissionList();
	}

	/*
	 * @Override public void onItemClick(AdapterView<?> parent, View view, int
	 * position, long id) {
	 * 
	 * AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
	 * 
	 * ((MissionEntry)parent.getItemAtPosition(position)).setTitle("Bajs " +
	 * position); view.requestLayout(); parent.requestLayout();
	 * 
	 * }
	 */

	@Override
	public void onTakePictureButtonClicked(int id) {
		_clickedMissionId = id;
		
		if (!User.hasCompleted(_clickedMissionId)) {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	
			Log.d("_pictureFileUri", "Before: " + _pictureFileUri);
			_pictureFileUri = createPictureUri(); // create a file to save the image
			
			Log.d("_pictureFileUri", "After: " + _pictureFileUri);
			Log.d("onPictureClick", "_pictureFileUri " + _pictureFileUri.getPath());
			intent.putExtra(MediaStore.EXTRA_OUTPUT, _pictureFileUri);
			
			startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
		}
	}

	@Override
	public void onGalleryButtonClicked(int id) {
		Log.d("onGalleryClick", "entry");
		String text = "You clicked gallery button for mission " + id;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(this, text, duration);
		toast.show();
	}

	// Unlocks menu button clicked.
	public void menuButtonUnlocks(View view) {

		_fragmentViewPager.setCurrentItem(0);
		_buttonManager.setSelection(0);

		TextView missionScreenTitle = (TextView) findViewById(R.id.screenTitle);
		missionScreenTitle.setText(R.string.screen_title_unlocks);
	}

	// Community menu button clicked.
	public void menuButtonCommunity(View view) {

		_fragmentViewPager.setCurrentItem(1, true);
		_buttonManager.setSelection(1);

		TextView missionScreenTitle = (TextView) findViewById(R.id.screenTitle);
		missionScreenTitle.setText(R.string.screen_title_community);
	}

	// Missions menu button clicked.
	public void menuButtonMission(View view) {

		_fragmentViewPager.setCurrentItem(2, true);
		_buttonManager.setSelection(3);

		_missionFragment.refreshMissionList();

		TextView missionScreenTitle = (TextView) findViewById(R.id.screenTitle);
		missionScreenTitle.setText(R.string.screen_title_mission);
	}

	// Profile menu button clicked
	public void menuButtonProfile(View view) {

		_fragmentViewPager.setCurrentItem(3, true);
		_buttonManager.setSelection(4);

		TextView missionScreenTitle = (TextView) findViewById(R.id.screenTitle);
		missionScreenTitle.setText(R.string.screen_title_profile);
	}

	private Uri createPictureUri() {
		return Uri.fromFile(createPictureFile());
	}

	private File createPictureFile() {

		File storageDirectory = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"Mamarazzi");

		if (storageDirectory.exists() == false) {
			boolean created = storageDirectory.mkdirs();
			if (created != true) {
				Log.d("createOutputPictureFileUri",
						"Could not create directory");
			}
		}

		String timestamp = new SimpleDateFormat("yyyyMMddHHmmss")
				.format(new Date());
		File pictureFile = new File(storageDirectory.getPath() + File.separator
				+ "MR_" + timestamp + ".jpg");

		Log.d("createPictureFile", "pictureFile: " + pictureFile.getPath());

		return pictureFile;
	}

	// Simple class for managing the backgrounds to be displayed on the menu
	// buttons on click.
	public class MenuButtonManager {
		private Context _context = null;
		ArrayList<View> _buttonList = new ArrayList<View>();

		public MenuButtonManager(Context context) {
			_context = context;
			_buttonList.add(((MainActivity) _context)
					.findViewById(R.id.menuButtonUnlock));
			_buttonList.add(((MainActivity) _context)
					.findViewById(R.id.menuButtonCommunity));
			_buttonList.add(((MainActivity) _context)
					.findViewById(R.id.menuButtonCamera));
			_buttonList.add(((MainActivity) _context)
					.findViewById(R.id.menuButtonMission));
			_buttonList.add(((MainActivity) _context)
					.findViewById(R.id.menuButtonProfile));
		}

		public void setSelection(int selection) {
			for (int i = 0; i < _buttonList.size(); i++) {
				if (i == selection)
					_buttonList.get(i).setBackgroundResource(
							R.color.selectedbutton);

				else
					_buttonList.get(i).setBackgroundResource(R.color.invisible);
			}
		}

	}
}
