package com.mamarazzi.game.activities;




import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mamarazzi.game.R;
import com.mamarazzi.game.classes.CameraPreview;

public class PhotoActivity extends Activity {

	private Camera _camera = null;
	private CameraPreview _preview = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Set fullscreen
		// Remove title bar
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_photo);
		
		Intent test = getIntent();
        int id = test.getIntExtra(MainActivity.MISSION_ID, -1);
		String text = "You clicked Take picture button for mission " + id;
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(this, text, duration);
		toast.show();
		
		// Set layout for activity
		
		
		_preview = new CameraPreview(this);
		_preview.setCamera(_camera);
		FrameLayout cameraFrame = (FrameLayout) findViewById(R.id.camera_frame);
		cameraFrame.addView(_preview);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.photo, menu);
		return true;
	}
	

    @Override
    protected void onPause() {
        super.onPause();

        // Because the Camera object is a shared resource, it's very
        // important to release it when the activity is paused.
        if (_camera != null) {
            _preview.stopPreviewAndFreeCamera();
            _camera = null;
        }
    }
    
    
    @Override
    protected void onResume() {
        super.onResume();
        
        

        // Open the default i.e. the first rear facing camera.
        _camera = safeGetCamera();
        _preview.setCamera(_camera);
    }
	
	
	private Camera safeGetCamera() {
		Camera camera = null;
		
		try {
			camera = Camera.open();
		}
		catch (Exception e) {
			Log.e("safeGetCamera", "Exception thrown in safeGetCamera. Error: " + e.getMessage());
		}
		
		return camera;
	}
	
	/*private void onSnap(View view) {
		takePicture(Camera.ShutterCallback shutter, Camera.PictureCallback raw, Camera.PictureCallback postview, Camera.PictureCallback jpeg)
	}*/
	

}
