package com.mamarazzi.game.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mamarazzi.game.R;
import com.mamarazzi.game.classes.CommentUploadTask;
import com.mamarazzi.game.classes.GetCommentsResponse;
import com.mamarazzi.game.classes.PictureUploadResponse;
import com.mamarazzi.game.classes.UploadTask;
import com.mamarazzi.game.classes.User;
import com.mamarazzi.game.classes.UserSettingsManager;

public class PictureUploadActivity extends Activity 
implements UploadTask.PictureUploadListener, CommentUploadTask.OnUploadCompleteListener {
	public static final String UPLOAD_URI = "uploadUri";
	private Bitmap _previewBitmap = null;
	private Bitmap _squarePreview = null;
	private int _missionId;
	private String _filePath = null;
	private EditText _commentField = null;
	private UploadTask _pictureUploadTask = null;
	private CommentUploadTask _commentUploadTask = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set fullscreen
		// Remove title bar
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_picture_upload);
		_commentField = (EditText) findViewById(R.id.commentField);
		
		
		
		
		Log.d("_activeUser", "" + User.getUsername() + " " + User.getId() );
		
		Intent uploadIntent = getIntent();
		Bundle uploadBundle = uploadIntent.getExtras();
		_missionId = uploadBundle.getInt("mission_id");
		
		_filePath = uploadIntent.getStringExtra(UPLOAD_URI);
		
	
		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(_filePath, bitmapOptions);
		
		int outHeight = bitmapOptions.outHeight;
		int outWidth = bitmapOptions.outWidth;
		
		Log.d("onCreate", "bitmapOptions.outHeight: " + outHeight);
		Log.d("onCreate", "bitmapOptions.outWidth: " + outWidth);
		
		bitmapOptions.inSampleSize = 4;
		bitmapOptions.inJustDecodeBounds = false;
		_previewBitmap = BitmapFactory.decodeFile(_filePath, bitmapOptions);
		
		outHeight = bitmapOptions.outHeight;
		outWidth = bitmapOptions.outWidth;
	
		if (outHeight >= outWidth) {
			int newY = (outHeight - outWidth) / 2;
			_squarePreview = Bitmap.createBitmap(_previewBitmap, 0, newY, outWidth, outWidth);
		}
		else {
			int newX = (outWidth - outHeight) / 2;
			_squarePreview = Bitmap.createBitmap(_previewBitmap, newX, 0, outHeight, outHeight);
		}
		_previewBitmap.recycle();
		
		_squarePreview = Bitmap.createScaledBitmap(_squarePreview, 512, 512, true);
		
		FileOutputStream compressStream = null;
		try {
			compressStream = new FileOutputStream(_filePath + "C");
			_squarePreview.compress(Bitmap.CompressFormat.PNG, 100, compressStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			try {
				compressStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		
		Log.d("onCreate", "bitmapCreated");
		
		ImageView imageView = (ImageView) findViewById(R.id.previewImageView);
		imageView.setImageBitmap(_squarePreview);
	}

	public void confirmPicture(View view) {
		_pictureUploadTask = new UploadTask(new File(_filePath + "C"), User.getId(), _missionId);
		_pictureUploadTask.setPictureUploadListener(this);
		_pictureUploadTask.execute();
		
		
		
		ImageButton uploadButton = (ImageButton) findViewById(R.id.uploadButton);
		ImageButton returnButton = (ImageButton) findViewById(R.id.uploadReturnButton);
		uploadButton.setVisibility(View.INVISIBLE);
		returnButton.setVisibility(View.INVISIBLE);
		
		TextView uploadText = (TextView) findViewById(R.id.uploadText);
		uploadText.setVisibility(View.VISIBLE);
		
	}
	
	public void returnToMain(View view) {
		setResult(RESULT_CANCELED, null);
		finish();
	}
	
	@Override
	protected void onPause () {
		super.onPause();
		if (_pictureUploadTask != null && !_pictureUploadTask.isCancelled()) {
			_pictureUploadTask.cancel(true);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.picture_upload, menu);
		return false;
	}

	@Override
	public void onPictureUploadComplete(PictureUploadResponse response) {
		if (response.successful()) {
			Log.d("PICTUREI ID", "" + response.getPictureId());
			_commentUploadTask = new CommentUploadTask(response.getPictureId(), User.getId(), _commentField.getText().toString());
			_commentUploadTask.setOnUploadCompleteListener(this);
			_commentUploadTask.execute();
		}
		else {
			Toast toast = Toast.makeText(this, "Something went wrong.", Toast.LENGTH_LONG);
			toast.show();
			
			ImageButton uploadButton = (ImageButton) findViewById(R.id.uploadButton);
			ImageButton returnButton = (ImageButton) findViewById(R.id.uploadReturnButton);
			uploadButton.setVisibility(View.VISIBLE);
			returnButton.setVisibility(View.VISIBLE);
			
			TextView uploadText = (TextView) findViewById(R.id.uploadText);
			uploadText.setVisibility(View.INVISIBLE);
		}
		
		
		
	}

	@Override
	public void onUploadComplete(GetCommentsResponse response) {
		if (response.successful()) {
			
			Bundle returnData = new Bundle();
			returnData.putString("return_message", "Picture successfuly uploaded!");
			returnData.putInt("mission_id", _missionId);
			Intent returnIntent = new Intent();
			returnIntent.putExtras(returnData);
			setResult(RESULT_OK, returnIntent);
			finish();
		}
		
		ImageButton uploadButton = (ImageButton) findViewById(R.id.uploadButton);
		ImageButton returnButton = (ImageButton) findViewById(R.id.uploadReturnButton);
		uploadButton.setVisibility(View.VISIBLE);
		returnButton.setVisibility(View.VISIBLE);
		
		TextView uploadText = (TextView) findViewById(R.id.uploadText);
		uploadText.setVisibility(View.INVISIBLE);
	}
	
	

}
