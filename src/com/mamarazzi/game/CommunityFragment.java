package com.mamarazzi.game;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mamarazzi.game.classes.CommentDownloadTask;
import com.mamarazzi.game.classes.GetCommentsResponse;
import com.mamarazzi.game.classes.MetaPicture;
import com.mamarazzi.game.classes.PictureMetaDownloadResponse;
import com.mamarazzi.game.classes.PictureMetaDownloadTask;
import com.mamarazzi.game.classes.User;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CommunityFragment 
extends Fragment 
implements PictureMetaDownloadTask.OnPictureMetaDownloadedListener,
ViewPager.OnPageChangeListener,
CommentDownloadTask.OnDownloadCompleteListener {
	
	private User _activeUser = null;
	private Activity _activityContext = null;
	private ViewPager _viewPager = null;
	private ImageAdapter _imageAdapter = null;
	private RelativeLayout _fragmentContainer = null;
	private ScrollView _currentScrollView = null;

	public CommunityFragment() {
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		_activityContext = activity;
	}
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		_imageAdapter = new ImageAdapter();
		
		for (int i = 0; i < 10; i++) {
			PictureMetaDownloadTask download = new PictureMetaDownloadTask();
			download.setOnPictureMetaDownloadedListener(this);
			download.execute();
		}
	
		
		Log.d("CommunityFragment", "onCreate");

		
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		RelativeLayout returnView = (RelativeLayout) inflater.inflate(R.layout.community_fragment_layout, container, false);
		ViewPager vp = (ViewPager) returnView.findViewById(R.id.view_pager);
		vp.setOnPageChangeListener(this);
		vp.setAdapter(_imageAdapter);

		_fragmentContainer = returnView;
		_viewPager = vp;
		
		return returnView;
	}
	
	
	public class ImageAdapter extends PagerAdapter {
		
		List<MetaPicture> _pictures = new ArrayList<MetaPicture>();

		public ImageAdapter() {
		}
		
		public void addMetaPicture(MetaPicture metaPicture) {
			_pictures.add(metaPicture);
			Log.d("Image add", "added image");
			notifyDataSetChanged();
		}
		
		public List<MetaPicture> getPictureMetaList() {
			return _pictures;
		}

		@Override
		public int getCount() {
			return _pictures.size();
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ScrollView prgrs = (ScrollView) _activityContext.getLayoutInflater().inflate(R.layout.community_progress_layout, container, false);
			MetaPicture metaPicture = _imageAdapter.getPictureMetaList().get(position);
			
			TextView username = (TextView) prgrs.findViewById(R.id.communityUsernameText);
			username.setText(metaPicture.getUsername());
			
			TextView missionTitle = (TextView) prgrs.findViewById(R.id.missionDescriptionText);
			missionTitle.setText(metaPicture.getTitle());
			
			ImageView image = (ImageView) prgrs.findViewById(R.id.image);
			DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.mamarazzilogo)
				.build();
			ImageLoader.getInstance().displayImage("http://mamarazzigame.com"+ _pictures.get(position).getImagePath(), image, options);
			
			((ViewPager) container).addView(prgrs, 0);
			
			return prgrs;
		}
		

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}
		
	}

	@Override
	public void onPictureMetaDownloaded(PictureMetaDownloadResponse result) {
		if (result.successful()) {
			for (int i = 0; i < result.getMetaPictures().size(); i++) {
				CommentDownloadTask downloader = new CommentDownloadTask(result.getMetaPictures().get(i).getId());
				downloader.setOnDownloadCompleteListener(this);
				downloader.execute();
				_imageAdapter.addMetaPicture(result.getMetaPictures().get(i));
			}
			
		}
		
	}
	
	@Override
	public void onDownloadComplete(GetCommentsResponse response) {
		int pictureId = response.getPictureId();
		for (MetaPicture metaPicture : _imageAdapter.getPictureMetaList()) {
			if (metaPicture.getId() == pictureId) {
				metaPicture.setComments(response.getComments());
				break;
			}
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionPixelOffset) {
		
	}

	@Override
	public void onPageSelected(int position) {
		
	}


	
}
